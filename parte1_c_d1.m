


num = [0 0 30 -3];
dem = [13 -5 -1 17];
funcion=tf(num, dem);

muestreo = 0.05;

[numd, demd]= c2dm(num, dem, muestreo, 'zoh');

[y] = dstep(numd, demd, 200+1);
x = 0:0.05:0.05*200;
stairs(x,y);

xlabel('Tiempo t');
ylabel('H(s)');
title('Grafico de la funcion de transferencia discretizada');
grid on;