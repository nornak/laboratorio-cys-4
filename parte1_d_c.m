

rango = 100;
muestreo_d = 0.08;
muestreo_c = 0.08;

num = [0 0 30 -3];
dem = [13 -5 -1 17];
funcion = tf(num, dem);

[numd, demd] = c2dm(num, dem, muestreo_d, 'zoh');

[numc, demc] = d2cm(numd, demd, muestreo_c, 'zoh');


step(numc, demc, rango);

xlabel('Tiempo t');
ylabel('H(s)');
title('Grafico de la funcion de transferencia transformada en el tiempo');
grid on;